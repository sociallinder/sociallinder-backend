package sociallinder.api.directServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import sociallinder.entity.ProjectEntity;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class Upload extends HttpServlet {




    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.addHeader("Access-Control-Allow-Origin", "*");


        Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
        List<BlobKey> blobKeys = blobs.get("myFile");
//        System.out.println(blobKeys.toString());

        //create imageurl
        Long projectId = Long.parseLong(req.getParameter("projectId"));
        ImagesService imagesService = ImagesServiceFactory.getImagesService();
        String url = imagesService.getServingUrl(blobKeys.get(0));

        //todo : save imageurl (stays the same!) to projectEntity
        try {
            ProjectEntity p = ofy().load().type(ProjectEntity.class).id(projectId).now();
            p.setImageUrl(url);
            ofy().save().entity(p).now();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        //construct response string
        String resp = "{\"blobkey\":\""+blobKeys.get(0).getKeyString()+"\"}";

        System.out.println(resp);

        res.setStatus(HttpServletResponse.SC_OK);
        res.setContentType("application/json");

        PrintWriter out = res.getWriter();
        out.print(resp);
        out.flush();
        out.close();
    }

}
