/**
 * 
 */
package sociallinder.entity;

import lombok.Data;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Saimadhav
 *
 */

@Data
@Index
@Entity
@Cache
public class OrganizationEntity {

	/**
	 * As usual the PK
	 */
	 @Id
	 private Long id;
	 
	 /**
	  * Title of the project
	  */
	 private String organizationTitle;
	 
	 /**
	  * Description of the project 
	  */
//	 private String organizationDescription;
	 
	 private String Address;
	 
	 private int phoneNumber;
	 
//	 private String email;

	 private Long organizationId;
	 
//	 private UserEntity ownerOfOrganization;

	 
}
