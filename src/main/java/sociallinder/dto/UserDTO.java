/**
 * 
 */
package sociallinder.dto;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;
import java.util.List;

import lombok.Data;
import sociallinder.entity.OrganizationEntity;
import sociallinder.entity.UserEntity;

/**
 * @author Saimadhav
 *
 */
@Data
public class UserDTO {

	public static UserDTO createSampleDto() {
		return null;
	}

	public UserDTO() {
	}

	public UserDTO(UserEntity user) {
		//populating all the attributes
		populateAttributes(user);
	}

	public UserDTO(long userID) {

		UserEntity user;
		user = ofy().load().type(UserEntity.class).id(userID).now();
		
		//populating all the attributes
		populateAttributes(user);
	}

	private void populateAttributes(UserEntity user) {
		userId = user.getUserId();
		userName = user.getUserName();
		mainGoal = user.getMainGoal();
		email = user.getEmail();
		password = user.getPassword();
		personalwebsite = user.getPersonalwebsite();
		Address = user.getAddress();
		totalFundedAmount = user.getTotalFundedAmount();
		birthDate = user.getBirthDate();
		hobbys = user.getHobbys();
		isOrganization = user.isOrganization();
		availability = user.getAvailability();

		if (isOrganization) {
			OrganizationEntity organizationEntity = ofy().load()
					.type(OrganizationEntity.class)
					.id(user.getOrganizationId()).now();
			OrganizationDTO dto = new OrganizationDTO(organizationEntity);
			organizationObject = dto;
		}

		projectType = user.getProjectType();
	}

	private Long userId;

	/**
	 * User Name
	 */
	private String userName;

	/**
	 * Main Aim of the user
	 */
	private String mainGoal;

	/**
	 * User Email id
	 */
	private String email;

	/**
	 * User password
	 */
	private String password;

	/**
	 * Personal website of the user
	 */
	private String personalwebsite;

	/**
	 * Address of the user
	 */
	private String Address;

	/**
	 * Total funded amount by user
	 */
	private Float totalFundedAmount;

	/**
	 * Date of birth
	 */
	private Date birthDate;

	/**
	 * User Hobbies
	 */
	private String hobbys;
	/**
	 * User type: Private Person = False, Organization = True;
	 */

	private boolean isOrganization;
	/**
	 * List of project types user are interested in
	 */
	private List<String> projectType;

	private OrganizationDTO organizationObject;

	/**
	 * Availability of the user
	 */
	private String availability;

}
