/**
 * This package shall include all "Domain Controller".
 * These classes contain business logic that is called from the API-Methods. Classes of this package
 * implement API-Methods and therefore deliver / save data to or from the front-end.
 */

package sociallinder.dc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import sociallinder.dto.SampleEntityDTO;
import sociallinder.entity.SampleEntity;

/**
 * Created by Timo on 08.10.15.
 *
 *
 */
public class SampleDC {

    public static SampleEntityDTO saveSampleEntity(String name){
        SampleEntity entity = new SampleEntity();
        entity.setName(name);

        ofy().save().entity(entity).now(); //do not forget the "now()" ! saving will create the id for us!

        SampleEntityDTO dto = new SampleEntityDTO(entity);

        return dto;
    }

    public static SampleEntityDTO readSampleEntity(long id){
        if (id <= 0){return null;}

        SampleEntity sampleEntity = ofy().load().type(SampleEntity.class).id(id).now();

        if(sampleEntity == null){return null;}

        SampleEntityDTO dto = new SampleEntityDTO(sampleEntity);
        return dto;
    }

    public static List<SampleEntityDTO> readAllSampleEntities(){
        List<SampleEntityDTO> result = new ArrayList<>();

        List<SampleEntity> list = ofy().load().type(SampleEntity.class).list();
        for (SampleEntity e : list){
            SampleEntityDTO dto = new SampleEntityDTO(e);
            result.add(dto);
        }

        return result;
    }
}
