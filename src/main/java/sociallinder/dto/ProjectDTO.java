/**
 * 
 */
package sociallinder.dto;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import sociallinder.dc.ProjectDC;
import sociallinder.dc.UserDC;
import sociallinder.entity.EventEntity;
import sociallinder.entity.ProjectEntity;

import com.googlecode.objectify.annotation.Load;

/**
 * @author Saimadhav
 *
 */
@Data
public class ProjectDTO {

	public ProjectDTO() {
	}

	public ProjectDTO(ProjectEntity project) {
		populateAttributes(project);
	}

	public ProjectDTO(long id) {
		ProjectEntity project = new ProjectEntity();
		project = ofy().load().type(ProjectEntity.class).id(id).now();
		id = project.getId();
		populateAttributes(project);
	}
	
	private void populateAttributes(ProjectEntity project) {
		id = project.getId();
		projectTitle = project.getProjectTitle();
		projectDescription = project.getProjectDescription();
		projectMission = project.getProjectMission();

		successFactors = new ArrayList<>();
		if (project.getSuccessFactors() != null) {
			for (String s : project.getSuccessFactors()) {
				MessageDTO dto = new MessageDTO(s);
				successFactors.add(dto);
			}
		}

		// DEPRECATED. create a keyneed instead.
		// totalAmount = project.getTotalAmount();
		// totalFunded = project.getTotalFunded();

		location = project.getLocation();
		projectType = project.getProjectType();
		organizationId = project.getOrganizationId();
		userId = project.getUserId();
		projectDate = project.getProjectDate();
		imageUrl = project.getImageUrl();

		keyNeeds = new ArrayList<>();
		if (project.getKeyNeeds() != null)
			for (Long id : project.getKeyNeeds()) {
				KeyNeedDTO dto = new KeyNeedDTO();
				dto = ProjectDC.getKeyNeedById(id);
				keyNeeds.add(dto);
			}

		supporters = new ArrayList<>();
		if (project.getSupporters() != null)
			for (Long id : project.getSupporters()) {
				DonationDTO dto = ProjectDC.getDonationById(id);
				supporters.add(dto);
			}

		// todo: iterate through events just like through keyneeds. will not
		// work otherwise.
		// todo: make sure all data is transferred to dto from entity
		
		startDate = project.getStartDate();
		endDate = project.getEndDate();
		
		if(project.getOrganizationId()!=0) {
			
			// organization needs to be implements
		} else if(project.getUserId()!=0){
			user = UserDC.getUserById(project.getUserId());
		}
		
	}

	public static ProjectDTO createSampleDto() {
		ProjectDTO dto = new ProjectDTO(ProjectEntity.createSampleEntity());
		return dto;
	}

	private Long id;

	/**
	 * Title of the project
	 */
	private String projectTitle;

	/**
	 * Description of the project
	 */
	private String projectMission;

	/**
	 * Description of the project
	 */
	private String projectDescription;

	/**
	 * success factors
	 */
	private List<MessageDTO> successFactors;

	/**
	 * Total amount required for the project
	 */
	// private double totalAmount;
	//
	// /**
	// * Total raisedfunds collected out of TotalAmount for the project
	// */
	// private double totalFunded;

	/**
	 * Address for the project funding
	 */
	private String location;

	/**
	 * Type of project eg: Refugee, Education etc.,
	 */
	private String projectType;

	/**
	 * Organization - who created the project and its information, A child
	 * Entity
	 */
	// private long organizationId;
	private long organizationId;

	/**
	 * User - who created the project and its information, A child Entity
	 */
	private long userId;

	/**
	 * This is date when the project is created
	 */
	private Date projectDate;

	/**
	 * used to associate the project image with the project.
	 */
//	private String imageBlobKey;
    private String imageUrl;

	/**
	 * shall replaces keyAspects
	 */
	@Load
	private List<KeyNeedDTO> keyNeeds;

	/**
	 * all events/tasks of the project
	 */
	@Load
	private List<EventEntity> events;

	/**
	 * list of supporter Ids
	 */

	private List<DonationDTO> supporters;

	/**
	 * User - who created the project and its information, A child Entity
	 */
	private UserDTO user;

	/**
	 * User - who created the project and its information, A child Entity
	 */
	private OrganizationDTO organization;

	/**
	 * This is date when the project is created
	 */
	private Date startDate;

	/**
	 * This is date when the project is created
	 */
	private Date endDate;

}
