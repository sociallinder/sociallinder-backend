package sociallinder.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import sociallinder.entity.SampleEntity;

/**
 * Created by Timo on 08.10.15.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString
public class SampleEntityDTO {
    private long id;
    private String name;

    public SampleEntityDTO(){}

    public SampleEntityDTO(SampleEntity e){
        this.id = e.getId();
        this.name = e.getName();
    }

   
}
