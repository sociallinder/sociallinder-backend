/**
 * 
 */
package sociallinder.api;

import java.util.List;

import javax.inject.Named;

import sociallinder.dc.UserDC;
import sociallinder.dto.UserDTO;
import sociallinder.util.Constants;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * @author D064747
 *
 */
@Api(name = "sociallinder", version = "v1", clientIds = { Constants.WEB_CLIENT_ID })
public class UserApi {
	@ApiMethod(path = "getAllUsers", httpMethod = ApiMethod.HttpMethod.GET)
	public List<UserDTO> getAllUsers() {
		return UserDC.getAllUsers();
	}

	@ApiMethod(path = "getUserById", httpMethod = ApiMethod.HttpMethod.GET)
	public UserDTO getUserById(@Named("id") long id) {
		return UserDC.getUserById(id);
	}
	
	@ApiMethod(path = "saveUserDetails", httpMethod = ApiMethod.HttpMethod.PUT)
	public UserDTO saveUserDetails(UserDTO user) {
		return UserDC.saveUserDetails(user);
	}
	
	@ApiMethod(path = "deleteUserById", httpMethod = ApiMethod.HttpMethod.DELETE)
	public void deleteUserById(@Named("id") long id) {
		UserDC.deleteUserById(id);
	}

	@ApiMethod(path = "createUser", httpMethod = ApiMethod.HttpMethod.POST)
	public UserDTO createUser(UserDTO project) {
		return UserDC.createUser(project);
	}
	
	@ApiMethod(path = "findUserByEmail", httpMethod = ApiMethod.HttpMethod.GET)
	public UserDTO findUserByEmail(@Named("email") String email, @Named("password") String password) {
		return UserDC.login(email,password);
	}
	@ApiMethod(path = "setUserProjectTypes", httpMethod = ApiMethod.HttpMethod.PUT)
	public UserDTO setUserProjectTypes(@Named("id") long id, @Named("Project Type") List <String> types) {
		return UserDC.setUserProjectTypes(id, types);
	}
}
