/**
 * 
 */
package sociallinder.dto;

import lombok.Data;
import sociallinder.entity.KeyAspectEntity;

/**
 * @author Saimadhav
 *
 */
@Data
public class KeyAspectsDTO {
	
	
	public KeyAspectsDTO(KeyAspectEntity keyAspects){
        id = keyAspects.getId();
        title = keyAspects.getTitle();
        description = keyAspects.getDescription();
        numberOfSupporters = keyAspects.getNumberOfSupporters();
    }
	 
	private long id;
	
	private String title;
	
	private String description;
	
	private int numberOfSupporters;
}
