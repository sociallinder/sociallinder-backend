/**
 * 
 */
package sociallinder.entity;

import lombok.Data;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Saimadhav
 *
 */

@Data
@Index
@Entity
@Cache
public class IdeaEntity {
	
	/**
	 * As usual the PK
	 */
	 @Id
	 private Long id;
	 
	 /**
	  * User who created the Idea
	  */
	 private UserEntity CreatedUser;
	 
	 /**
	  * main concept
	  */
	 private String concept;

}
