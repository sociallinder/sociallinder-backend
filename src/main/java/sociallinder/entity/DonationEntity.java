package sociallinder.entity;


import lombok.Data;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Marouane
 *
 */
@Index
@Data
@Entity
@Cache

public class DonationEntity {
	
	public static DonationEntity createsampleDonation(){
		DonationEntity e = new DonationEntity();
		e.id=123L;
		e.Amount= 11;
		e.KeyNeedTitle= "Some Title";
		e.KeyNeedID=123L;
		e.userId=123L;
		return e;
		
		
	}
	
	
	/*
	 * PK 
	*/
	@Id
	private Long id;
	 
	/*
	 * DonationType: either money resource or time
	 */
	private Long KeyNeedID;
	/*
	 * Description of the donation
	 */
	private String KeyNeedTitle;
	
	/*
	 * User who made the donation
	 */
	private Long userId;
	
	/*
	 * Amount needed/donated
	 */
	
	private double Amount;
	
	
	
}
