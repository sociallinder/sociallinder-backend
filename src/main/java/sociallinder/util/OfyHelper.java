package sociallinder.util;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import sociallinder.entity.*;

import com.googlecode.objectify.ObjectifyService;

/**
 * OfyHelper, a ServletContextListener, is setup in web.xml to run before a JSP is run.  This is
 * required to let JSP's access Ofy.
 **/
public class OfyHelper implements ServletContextListener {
    public void contextInitialized(ServletContextEvent event) {
        // This will be invoked as part of a warmup request, or the first user
        // request if no warmup request was invoked.
//        ObjectifyService.register(Guestbook.class);
        System.out.println("[OfyHelper] registering Models...");

        ObjectifyService.register(SampleEntity.class);
        ObjectifyService.register(ProjectEntity.class);
        ObjectifyService.register(KeyNeedEntity.class);
        ObjectifyService.register(IdeaEntity.class);
        ObjectifyService.register(OrganizationEntity.class);
        ObjectifyService.register(SuccessStoryEntity.class);
        ObjectifyService.register(UserEntity.class);
        ObjectifyService.register(KeyAspectEntity.class);
        ObjectifyService.register(DonationEntity.class);

    }

    public void contextDestroyed(ServletContextEvent event) {
        // App Engine does not currently invoke this method.
    }



}

