package sociallinder.entity;


import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Index
@Cache
public class LikeEntity {

    @Id
    private Long id;

    private Long userId;

    private Date createdDate;

    public static LikeEntity createSampleEntity() {
        LikeEntity entity = new LikeEntity();
        entity.id = 123L;
        entity.userId = 566L;
        entity.createdDate = new Date();

        return entity;
    }
}