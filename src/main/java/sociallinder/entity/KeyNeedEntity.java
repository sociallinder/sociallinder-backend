package sociallinder.entity;

import lombok.Data;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Data
@Entity
@Index
@Cache
public class KeyNeedEntity {

	@Id
	private Long id;

	private String title;

	private String description;

	private double totalNeeds;

	private double needsMet;

	private String dimension; // Either: MONEY,TIME,RESOURCE or SAMPLE (for our samples)

	public static KeyNeedEntity createSampleEntity() {
		KeyNeedEntity entity = new KeyNeedEntity();
		entity.id = 123L;
		entity.title = "Some Keyneed";
		entity.description = "Some keyneed description";
		entity.totalNeeds = 10;
		entity.needsMet = 2;
		entity.dimension = "Sample";

		return entity;
	}

}