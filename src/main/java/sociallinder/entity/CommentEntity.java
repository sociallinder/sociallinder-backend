package sociallinder.entity;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import lombok.Data;

import java.util.Date;

/**
 * Created by Timo on 25.10.15.
 */

@Data
@Entity
@Index
@Cache
public class CommentEntity {

    @Id
    private Long id;

    private String message;

    private Long userId;

    private Date createdDate;

    public static CommentEntity createSampleEntity() {
        CommentEntity entity = new CommentEntity();
        entity.id = 123L;
        entity.message = "Some Comment";
        entity.userId = 456L;
        entity.createdDate = new Date();

        return entity;
    }
}
