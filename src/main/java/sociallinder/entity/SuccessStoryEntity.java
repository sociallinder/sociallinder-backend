/**
 * 
 */
package sociallinder.entity;

import java.util.List;

import lombok.Data;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Saimadhav
 *
 */

@Data
@Entity
@Cache
@Index
public class SuccessStoryEntity {
	
	/**
	 * As usual the PK
	 */
	 @Id
	 private Long id;
	 
	 private String title;
	 
	 private String description;
	 
	 private List<String> users;

}
