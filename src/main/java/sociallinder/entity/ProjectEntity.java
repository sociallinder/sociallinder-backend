/**
 * 
 */
package sociallinder.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.*;
import lombok.Data;

/**
 * @author Saimadhav
 *
 */

@Data
@Entity
@Index
@Cache
public class ProjectEntity {

    public static ProjectEntity createSampleEntity(){
        ProjectEntity entity = new ProjectEntity();
        entity.id = 12345L;
        entity.projectTitle = "One Sample Title";
        entity.projectMission = "a Sample Mission";
        entity.projectDescription = "Some Sample Description";
        entity.successFactors = new ArrayList<>();
        entity.successFactors.add("One Successfactor");
        entity.successFactors.add("Second Successfactor");
        entity.location = "Some Location";
        entity.projectType = "Some Projecttype";
        entity.projectDate = new Date();
//        entity.imageBlobKey = "SOME_BLOB_KEY";
        entity.keyNeeds = new ArrayList<>();
        KeyNeedEntity need1 = KeyNeedEntity.createSampleEntity();
        KeyNeedEntity need2 = KeyNeedEntity.createSampleEntity();
        entity.keyNeeds.add(need1.getId());
        entity.keyNeeds.add(need2.getId());
        EventEntity event1 = EventEntity.createSampleEntity();
        EventEntity event2 = EventEntity.createSampleEntity();
        entity.events = new ArrayList<>();
        entity.events.add(event1.getId());
        entity.events.add(event2.getId());

        return entity;
    }
	
	/**
	 * As usual the PK
	 */
	 @Id
	 private Long id;
	 
	 /**
	  * Title of the project
	  */
	 private String projectTitle;

    /**
     * Description of the project
     */
    private String projectMission;

	 /**
	  * Description of the project 
	  */
	 private String projectDescription;

    /**
     * success factors
     */
    private List<String> successFactors;

	 /**
	  * Total amount required for the project
	  */
//	 private double totalAmount;
//
//	 /**
//	  * Total raisedfunds collected out of TotalAmount for the project
//	  */
//	 private double totalFunded;
	 
	 /**
	  * Address for the project funding
	  */
	 private String location;
	 
	 /**
	  * Type of project eg: Refugee, Education etc.,
	  */
	 private String projectType;
	 
	 
	 /**
	  * Organization - who created the project and its information, A child Entity
	  */
	 private long organizationId;
	 
	 /**
	  * User - who created the project and its information, A child Entity
	  */
	 private long userId;
	 /**
	  * This is date when the project is created
	  */
	 private Date projectDate;
	 /**
	  * Key Needs
	  */
	 private KeyAspectEntity keyAspects;

    /**
     * used to associate the project image with the project.
     */
//    private String imageBlobKey;
    private String imageUrl;

    /**
     * shall replaces keyAspects
     */
    @Load
    private List<Long> keyNeeds;

    /**
     * all events/tasks of the project
     */
    @Load
    private List<Long> events;

    /**
     * list of supporter Ids
     */

    private List<Long> supporters;
    
    /**
	 * This is date when the project is created
	 */
	private Date startDate;
	
	/**
	 * This is date when the project is created
	 */
	private Date endDate;
}

