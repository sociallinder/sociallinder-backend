package sociallinder.util;

/**
 * Contains the client IDs and scopes for allowed clients consuming your API.
 */
public class Constants {
    //client key: HoGP3A0lS132SgrN6mAfbmED
    public static final String WEB_CLIENT_ID = "921637305303-8du0nnddmg0gmiqtp8v3bfc6sc6rak2o.apps.googleusercontent.com";
  public static final String ANDROID_CLIENT_ID = "replace this with your Android client ID";
  public static final String IOS_CLIENT_ID = "replace this with your iOS client ID";
  public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}

/*
 * stuff that might be useful:
 *
 * https://medium.com/google-cloud/uploading-to-google-cloud-storage-from-ios-app-e9f4097b516
 * http://stackoverflow.com/questions/25412119/uploading-an-image-from-an-external-link-to-google-cloud-storage-using-google-ap
 *
 * https://cloud.google.com/appengine/docs/java/googlecloudstorageclient/
 * https://cloud.google.com/appengine/docs/java/blobstore/
 * https://cloud.google.com/appengine/docs/java/images/
 * --> upload image to google cloud storage through webservice
 *
 */
