/**
 * 
 */
package sociallinder.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import sociallinder.entity.OrganizationEntity;


/**
 * @author Saimadhav
 *
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString
public class OrganizationDTO {
    
    public static OrganizationDTO createSampleDto() {
        return null;
    }

	public OrganizationDTO(){}

    public OrganizationDTO(OrganizationEntity organization){
        id = organization.getId();
        organizationTitle = organization.getOrganizationTitle();
        address = organization.getAddress();
        phoneNumber = organization.getPhoneNumber();

    }

	private Long id;

	/**
	 * Title of the project
	 */
	private String organizationTitle;

	/**
	 * Description of the project
	 */
//	private String organizationDescription;
    
    /**
     * Address of Organization
     */
	private String address;

    /**
     * phone number of Organization
     */
	private int phoneNumber;

    /**
     * email id
     */
//	private String email;

    /**
     * owner Of Organization
     */
//	private UserDTO ownerOfOrganization;

}
