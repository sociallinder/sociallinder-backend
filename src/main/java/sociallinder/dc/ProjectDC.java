/**
 * 
 */
package sociallinder.dc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sociallinder.dto.DonationDTO;
import sociallinder.dto.KeyNeedDTO;
import sociallinder.dto.MessageDTO;
import sociallinder.dto.ProjectDTO;
import sociallinder.dto.UserDTO;
import sociallinder.entity.DonationEntity;
import sociallinder.entity.KeyNeedEntity;
import sociallinder.entity.ProjectEntity;

/**
 * @author Saimadhav
 *
 */
public class ProjectDC {

	/**
	 * Get the List of projects available
	 * 
	 * @return
	 */
	public static List<ProjectDTO> getAllProjects() {
		List<ProjectDTO> result = new ArrayList<>();

		List<ProjectEntity> list = ofy().load().type(ProjectEntity.class).list();
		ProjectDTO projectDTO = null;
		for (ProjectEntity e : list) {
			projectDTO = new ProjectDTO(e);
			result.add(projectDTO);
		}
		return result;
	}

	/**
	 * Find the project based on ID
	 * 
	 * @param id
	 * @return
	 */
	public static ProjectDTO getProjectById(long id) {

		if (id <= 0) {
			return null;
		}

		ProjectEntity project = ofy().load().type(ProjectEntity.class).id(id).now();

		if (project == null) {
			return null;
		}

		ProjectDTO dto = new ProjectDTO(project);
		return dto;
	}
	
	
	
	public static KeyNeedDTO getKeyNeedById(long id) {

		if (id <= 0) {
			return null;
		}

		KeyNeedEntity project = ofy().load().type(KeyNeedEntity.class).id(id).now();

		if (project == null) {
			return null;
		}

		KeyNeedDTO dto = new KeyNeedDTO(project);
		return dto;
	}
	
	public static DonationDTO getDonationById(long id) {

		if (id <= 0) {
			return null;
		}

		DonationEntity donation = ofy().load().type(DonationEntity.class).id(id).now();

		if (donation == null) {
			return null;
		}

		DonationDTO dto = new DonationDTO(donation);
		return dto;
	}
	public static List<ProjectDTO> getProjectbyprojectType(List <String> ProjectType){
		List <ProjectEntity> project;
		project = ofy().load().type(ProjectEntity.class).list();
		List <ProjectDTO> list = new ArrayList <>();
		for(String type: ProjectType){
			if(project==null)
				return null;
			for(ProjectEntity p: project){
				if(p.getProjectType()!=null){
					
					if(p.getProjectType().compareToIgnoreCase(type)==0){
						ProjectDTO dto = new ProjectDTO(p.getId());
						if(!list.contains(dto))
							list.add(dto);
					}
				}
			}
		}
		return list;
	}
	public static KeyNeedDTO addKeyNeedsbyId(KeyNeedDTO dto){
			
			KeyNeedEntity e = ofy().load().type(KeyNeedEntity.class).id(dto.getId()).now();
			
	//		if(dto!= null){
	//			List<KeyNeedEntity> keyneeds = dto.getKeyNeeds();
	//		}
				
	
	            e.setTitle(dto.getTitle());
	            e.setDescription(dto.getDescription());
	            e.setTotalNeeds(dto.getTotalNeeds());
	            e.setNeedsMet(dto.getNeedsMet());
	            e.setDimension(dto.getDimension());
	            ofy().save().entity(e).now();
	            
	    		return dto;
	
		}
	public static List <KeyNeedDTO> getKeyNeedsbyprojectid(long projectid){
		
		List <KeyNeedDTO> dto = new ArrayList <>();
		ProjectEntity p =  ofy().load().type(ProjectEntity.class).id(projectid).now();
		if(p==null)
			return null;
		if(p.getKeyNeeds()==null)
			return null;
		for(long id: p.getKeyNeeds()){
			KeyNeedDTO d = new KeyNeedDTO(id);
			dto.add(d);
		}
		
		return dto;
		
		
	}
	public static List <UserDTO> getSupportersbyprojectid(long projectid){
		List <UserDTO> dto = new ArrayList <>();
		ProjectEntity p =  ofy().load().type(ProjectEntity.class).id(projectid).now();
		if(p==null)
			return null;
		if(p.getSupporters()==null)
			return null;
		for(long id: p.getSupporters()){
			DonationDTO d = new DonationDTO(id);
			UserDTO u = new UserDTO(d.getUserId());
			if(!dto.contains(u))
				dto.add(u);			
		}
		return dto;
	}
	
	public static MessageDTO getNumberofSupportersbyprojectid(long projectid){
				
		MessageDTO n = new MessageDTO();
        String M = "0";
        List<UserDTO> res = ProjectDC.getSupportersbyprojectid(projectid);
        if (res != null){
            M = ""+res.size();
        }
		n.setMessage(M);
		return n;
	}
	
	public static MessageDTO getPercentageNeedMet(long projectid){
		
		MessageDTO n = new MessageDTO();
		//double TotalNeedMet=0;
		//double TotalNeeds=0;
		double p;
        double pSum = 0;
		
		List <KeyNeedDTO> K = ProjectDC.getKeyNeedsbyprojectid(projectid);
		if(K==null)
			return null;
		for(KeyNeedDTO dto: K){
			//TotalNeedMet += dto.getNeedsMet();
			//TotalNeeds+= dto.getTotalNeeds();
            pSum += (dto.getNeedsMet() / dto.getTotalNeeds());
		}
		p = Math.round((pSum / K.size()) * 100);
//		p=Math.round((TotalNeedMet/TotalNeeds)*100);
		String Message = ""+p;
		n.setMessage(Message);
		return n;
	}

	/**
	 * Save the New project
	 */
	public static ProjectDTO createProject(ProjectDTO project) {

		ProjectEntity p = new ProjectEntity();
        p.setProjectTitle(project.getProjectTitle());
        p.setProjectMission(project.getProjectMission());
        p.setProjectDescription(project.getProjectDescription());

        //DEPRECATED. create a keyneed instead!
//        p.setTotalAmount(project.getTotalAmount());
//        p.setTotalFunded(project.getTotalFunded());

        p.setOrganizationId(project.getOrganizationId());
        p.setUserId(project.getUserId());

        List<String> successFactors = new ArrayList<>();
        if(project.getSuccessFactors()!=null)
        for (MessageDTO messageDTO : project.getSuccessFactors()) {
            String s = messageDTO.getMessage();
            successFactors.add(s);
        }
        p.setSuccessFactors(successFactors);
        
        p.setLocation(project.getLocation());
        p.setProjectType(project.getProjectType());
        p.setOrganizationId(project.getOrganizationId());
        p.setUserId(project.getUserId());

        if(project.getStartDate() != null) {
            p.setStartDate(project.getStartDate());
        }
        else{
            p.setStartDate(new Date());
        }
        p.setEndDate(project.getEndDate());
       
        
        List<Long> keyneeds = new ArrayList<>();
        if(project.getKeyNeeds()!=null)
        for (KeyNeedDTO dto : project.getKeyNeeds()) {
            KeyNeedEntity e = new KeyNeedEntity();
            e.setTitle(dto.getTitle());
            e.setDescription(dto.getDescription());
            e.setTotalNeeds(dto.getTotalNeeds());
            e.setNeedsMet(dto.getNeedsMet());
            e.setDimension(dto.getDimension());
            ofy().save().entity(e).now();
            keyneeds.add(e.getId());
        }
        
        
        p.setKeyNeeds(keyneeds);
        List<Long> donations = new ArrayList<>();
        if(project.getSupporters()!=null)
        for(DonationDTO dto : project.getSupporters()){
        	DonationEntity d = new DonationEntity ();
        	d.setAmount(dto.getAmount());
        	d.setKeyNeedID(dto.getKeyNeedID());
        	d.setKeyNeedTitle(dto.getKeyNeedTitle());
        	d.setUserId(dto.getUserId());
        	ofy().save().entity(d);
        	donations.add(d.getId());
        }
        p.setSupporters(donations);
  //      p.setEvents(project.getEvents());
		ofy().save().entity(p).now();

        return new ProjectDTO(p);
	}

	public static void deleteProjectById(long id) {
		if (id <= 0) {
			return;
		}
		
		ProjectEntity project = ofy().load().type(ProjectEntity.class).id(id).now();
		if (project == null) {
			return;
		}
		ofy().delete().entities(project).now();
	}
	
	public static List<ProjectDTO> getProjectbyUserId(long userId) {
		List<ProjectDTO> result = new ArrayList<>();
		
		List<ProjectEntity> list = ofy().load().type(ProjectEntity.class)
				.filter("userId", userId).list();

		ProjectDTO ProjectDTO = null;
		for (ProjectEntity e : list) {
			ProjectDTO = new ProjectDTO(e);
			result.add(ProjectDTO);
		}
		return result;
	}
	public static DonationDTO SupportProject(long projectid, long keyneedid, DonationDTO dto){
		
		ProjectEntity p = ofy().load().type(ProjectEntity.class).id(projectid).now();
		ofy().load().type(KeyNeedEntity.class).id(keyneedid).now();
		DonationEntity d = new DonationEntity();
		
		d.setAmount(dto.getAmount());
		d.setKeyNeedID(keyneedid);
		d.setKeyNeedTitle(dto.getKeyNeedTitle());
		d.setUserId(dto.getUserId());
		ofy().save().entity(d).now();
				
		if(p.getSupporters()==null){
			List <Long> s = new ArrayList<>();
			s.add(d.getId());
			p.setSupporters(s);
		}else{
			p.getSupporters().add(d.getId());
		}

		ofy().save().entity(p).now();

		return dto;
	}
	
	

}
