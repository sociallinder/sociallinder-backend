package sociallinder.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Timo on 06.10.15.
 */

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"message"})
@AllArgsConstructor
@ToString
public class MessageDTO {
    private String message;
}

