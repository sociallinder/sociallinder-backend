/**
 * 
 */
package sociallinder.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Saimadhav
 *
 */

@Data
@Entity
@Cache
@Index
public class UserEntity {

	/**
	 * As usual the PK
	 */
	@Id
	private Long userId;

	/**
	 * User Name
	 */

	private String userName;

	/**
	 * Main Aim of the user
	 */

	private String mainGoal;

	/**
	 * User Email id
	 */

	private String email;

	/**
	 * User password
	 */

	private String password;

	/**
	 * Personal website of the user
	 */

	private String personalwebsite;

	/**
	 * Address of the user
	 */

	private String Address;

	/**
	 * Total funded amount by user
	 */

	private Float totalFundedAmount;

	/**
	 * Date of birth
	 */
	private Date birthDate;

	/**
	 * User Hobbies
	 */
	private String hobbys;
	/**
	 * User type: Private Person = False, Organization = True;
	 */
	private boolean isOrganization;

	private Long organizationId;

	/**
	 * List of project types user are interested in
	 */
	private List<String> projectType;

	/**
	 * Availability of the user
	 */
	private String availability;

}
