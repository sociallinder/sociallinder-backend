package sociallinder.entity;

import com.googlecode.objectify.annotation.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Timo on 25.10.15.
 */

@Data
@Entity
@Index
@Cache
public class EventEntity {

    @Id
    private Long id;

    private String title;

    private String description;

    @Load
    private List<CommentEntity> comments;

    @Load
    private List<LikeEntity> likes;

    @Load
    private List<KeyNeedEntity> keyNeeds;

    /**
     * list of supporter Ids
     */

    private List<Long> supporters;


    public static EventEntity createSampleEntity() {
        EventEntity entity = new EventEntity();
        entity.id = 456L;
        entity.title = "Some Event";
        entity.description = "Some Event description";
        CommentEntity comment1 = CommentEntity.createSampleEntity();
        CommentEntity comment2 = CommentEntity.createSampleEntity();
        entity.comments = new ArrayList<>();
        entity.comments.add(comment1);
        entity.comments.add(comment2);
        LikeEntity like1 = LikeEntity.createSampleEntity();
        LikeEntity like2 = LikeEntity.createSampleEntity();
        entity.likes = new ArrayList<>();
        entity.likes.add(like1);
        entity.likes.add(like2);
        entity.keyNeeds = new ArrayList<>();
        KeyNeedEntity need1 = KeyNeedEntity.createSampleEntity();
        KeyNeedEntity need2 = KeyNeedEntity.createSampleEntity();
        entity.keyNeeds.add(need1);
        entity.keyNeeds.add(need2);

        return entity;
    }
}
