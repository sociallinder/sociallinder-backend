/**
 * 
 */
package sociallinder.api;

import java.util.List;

import javax.inject.Named;

import lombok.Lombok;
import sociallinder.dc.ProjectDC;
import sociallinder.dto.DonationDTO;
import sociallinder.dto.KeyNeedDTO;
import sociallinder.dto.MessageDTO;
import sociallinder.dto.ProjectDTO;
import sociallinder.dto.UserDTO;
import sociallinder.util.Constants;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

/**
 * @author Saimadhav
 *
 */
@Api(name = "sociallinder", version = "v1", clientIds = { Constants.WEB_CLIENT_ID })
public class ProjectApi {

    public static final String CLOUD_STORAGE_ID = "pink-elephnt.appspot.com";
//    public static final String CLOUD_STORAGE_ID = "sociallinder.appspot.com";


    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	@ApiMethod(path = "getAllProjects", httpMethod = ApiMethod.HttpMethod.GET)
	public List<ProjectDTO> getAllProjects() {
		return ProjectDC.getAllProjects();
	}

	@ApiMethod(path = "getProjectById", httpMethod = ApiMethod.HttpMethod.GET)
	public ProjectDTO getProjectById(@Named("id") long id) {
		return ProjectDC.getProjectById(id);
	}
	
	@ApiMethod(path = "deleteProjectById", httpMethod = ApiMethod.HttpMethod.DELETE)
	public void deleteProjectById(@Named("id") long id) {
		ProjectDC.deleteProjectById(id);
	}

	@ApiMethod(path = "saveProject", httpMethod = ApiMethod.HttpMethod.POST)
	public ProjectDTO saveProject(ProjectDTO project) {
		
		return ProjectDC.createProject(project);
	}
	
	@ApiMethod(path = "getProjectbyUserId", httpMethod = ApiMethod.HttpMethod.POST)
	public List<ProjectDTO> getProjectbyUserId(@Named("UserId") long UserId) {
		
		return ProjectDC.getProjectbyUserId(UserId);
	}
	
	@ApiMethod(path = "SupportProject", httpMethod = ApiMethod.HttpMethod.POST)
	public DonationDTO SupportProject(@Named("projectid") long projectid,@Named("keyneedid") long keyneedid, DonationDTO dto) {
		
		return ProjectDC.SupportProject(projectid, keyneedid, dto);
	}
	
	@ApiMethod(path = "getKeyNeedById", httpMethod = ApiMethod.HttpMethod.GET)
	public KeyNeedDTO getKeyNeedById(@Named("id") long id) {
		
		return ProjectDC.getKeyNeedById(id);
	}
	
	@ApiMethod(path = "getKeyNeedsByProjectID", httpMethod = ApiMethod.HttpMethod.GET)
	public List <KeyNeedDTO> getKeyNeedsByProjectID(@Named("ProjectID") long id) {
		
		return ProjectDC.getKeyNeedsbyprojectid(id);
	}
	
	@ApiMethod(path = "getSupportersByProjectID", httpMethod = ApiMethod.HttpMethod.GET)
	public List <UserDTO> getSupportersByProjectID(@Named("ProjectID") long id) {
		
		return ProjectDC.getSupportersbyprojectid(id);
	}
	@ApiMethod(path = "getProjectByProjectType", httpMethod = ApiMethod.HttpMethod.GET)
	public List <ProjectDTO> getProjectByProjectType(@Named("Project Type") List<String> type) {
		
		return ProjectDC.getProjectbyprojectType(type);
	}
	@ApiMethod(path = "getNumberSupportersByProjectID", httpMethod = ApiMethod.HttpMethod.GET)
	public MessageDTO getNumberOfSupportersByProjectID(@Named("ProjectID") long id) {
		
		return ProjectDC.getNumberofSupportersbyprojectid(id);
	}
	@ApiMethod(path = "getNumberPercentageNeedMetByProjectID", httpMethod = ApiMethod.HttpMethod.GET)
	public MessageDTO getPercentageNeedMetByProjectID(@Named("ProjectID") long id) {
		
		return ProjectDC.getPercentageNeedMet(id);
	}
    @ApiMethod(path = "getImageUploadCallbackUrl", httpMethod = ApiMethod.HttpMethod.GET)
    public MessageDTO getImageUploadCallbackUrl(@Named("projectId") long projectId){

        String uploadUrl = this.blobstoreService
                .createUploadUrl("/upload?projectId="+projectId,
                        UploadOptions.Builder.withGoogleStorageBucketName(CLOUD_STORAGE_ID));

        return new MessageDTO(uploadUrl);
    }
    
    @ApiMethod(path = "addKeyNeedsbyId", httpMethod = ApiMethod.HttpMethod.PUT)
	public KeyNeedDTO addKeyNeedsbyId(KeyNeedDTO project) {
		
		return ProjectDC.addKeyNeedsbyId(project);
	}
    
/*    @ApiMethod(path = "getPercentageOfNeedsMetbyProjectId", httpMethod = ApiMethod.HttpMethod.GET)
    public MessageDTO getPercentageOfNeedsMetbyProjectId(@Named("projectId") long projectId){
    	
    	String P=  ProjectDC.getPercentageOfNeedsMetbyProjectId(projectId);

       return new MessageDTO(P);

    }
*/

    @ApiMethod(path = "serveImage", httpMethod = ApiMethod.HttpMethod.GET)
    public MessageDTO serveImage(@Named("blobkey") String blobKey){
        BlobKey blobK = new BlobKey(blobKey);
        System.out.println(blobKey);
        ImagesService imagesService = ImagesServiceFactory.getImagesService();
        ServingUrlOptions servingUrlOptions = ServingUrlOptions.Builder.withBlobKey(blobK);
        String imageBlobKey = imagesService.getServingUrl(servingUrlOptions);

        return new MessageDTO(imageBlobKey);
    }

}

/**
 * some resources for image upload:
 * http://ikaisays.com/2010/09/08/gwt-blobstore-the-new-high-performance-image-serving-api-and-cute-dogs-on-office-chairs/
 *
 *https://cloud.google.com/appengine/docs/python/blobstore/#Python_Using_the_Blobstore_API_with_Google_Cloud_Storage
 *
 * http://stackoverflow.com/questions/16246734/using-blobstore-with-google-cloud-endpoint-and-android
 */
