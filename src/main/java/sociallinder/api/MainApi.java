package sociallinder.api;

import java.util.List;

import javax.inject.Named;

import sociallinder.dc.SampleDC;
import sociallinder.dto.*;
import sociallinder.util.Constants;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * Add your first API methods in this class, or you may create another class. In
 * that case, please update your web.xml accordingly.
 **/
@Api(name = "sociallinder", version = "v1", clientIds = { Constants.WEB_CLIENT_ID })
public class MainApi {

	@ApiMethod(path = "helloworld", httpMethod = ApiMethod.HttpMethod.GET)
	public MessageDTO helloWorld() {
		return new MessageDTO("Hello, World!");
	}

	@ApiMethod(path = "saveSampleEntity", httpMethod = ApiMethod.HttpMethod.GET)
	public SampleEntityDTO saveSampleEntity(@Named("name") String name) {
		return SampleDC.saveSampleEntity(name);
	}

	@ApiMethod(path = "readSampleEntity", httpMethod = ApiMethod.HttpMethod.GET)
	public SampleEntityDTO readSampleEntity(@Named("id") long id) {
		return SampleDC.readSampleEntity(id);
	}

	@ApiMethod(path = "readAllSampleEntities", httpMethod = ApiMethod.HttpMethod.GET)
	public List<SampleEntityDTO> readAllSampleEntities() {
		return SampleDC.readAllSampleEntities();

	}

    @ApiMethod(path = "showSampleProjectDTO", httpMethod = ApiMethod.HttpMethod.GET)
    public ProjectDTO showSampleProjectDTO(){
        return ProjectDTO.createSampleDto();
    }


    @ApiMethod(path = "showSampleOrganizationDTO", httpMethod = ApiMethod.HttpMethod.GET)
    public OrganizationDTO showSampleOrganizationDTO(){
        return OrganizationDTO.createSampleDto();
    }

    @ApiMethod(path = "showSampleUserDTO", httpMethod = ApiMethod.HttpMethod.GET)
    public UserDTO showSampleUserDTO(){
        return UserDTO.createSampleDto();
    }


}
