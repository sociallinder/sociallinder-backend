/**
 * 
 */
package sociallinder.entity;

import lombok.Data;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * @author Saimadhav
 *
 */
@Index
@Data
@Entity
@Cache
public class KeyAspectEntity {

	@Id
	private long id;

	private String title;

	private String description;

	private int numberOfSupporters;

	/**
	 * Key Needs
	 */
	@Parent
	private Ref<ProjectEntity> project;
}
