package sociallinder.dto;



import static com.googlecode.objectify.ObjectifyService.ofy;
import lombok.Data;
import sociallinder.entity.DonationEntity;


/**
 * @author Marouane
 *
 */

@Data
public class DonationDTO {
	
	private Long id;
	
	private String KeyNeedTitle;

	private Long KeyNeedID;
	
	private Long userId;
	
	private double Amount;
	
	public DonationDTO(){}
	
	public DonationDTO(DonationEntity d){
		
		this.id = d.getId();
		this.KeyNeedTitle=d.getKeyNeedTitle();
		this.KeyNeedID= d.getKeyNeedID();
		this.userId=d.getUserId();
		this.Amount=d.getAmount();		
	}
	public DonationDTO(long donationID){
		
		DonationEntity d = new DonationEntity();
		d=ofy().load().type(DonationEntity.class).id(donationID).now();
		this.id = d.getId();
		this.KeyNeedTitle=d.getKeyNeedTitle();
		this.KeyNeedID= d.getKeyNeedID();
		this.userId=d.getUserId();
		this.Amount=d.getAmount();	
		
	}
	public static DonationDTO createSampleEntity(){
		DonationDTO d = new DonationDTO(DonationEntity.createsampleDonation());
		return d;
	}
}
