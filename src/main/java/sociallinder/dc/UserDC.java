/**
 * 
 */
package sociallinder.dc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import sociallinder.dto.OrganizationDTO;
import sociallinder.dto.UserDTO;
import sociallinder.entity.OrganizationEntity;
import sociallinder.entity.UserEntity;

/**
 * @author Saimadhav
 *
 */
public class UserDC {

	private static final Logger log = Logger.getLogger(UserDC.class.getName());

	/**
	 * Find all the users
	 * 
	 * @return
	 */
	public static List<UserDTO> getAllUsers() {
		List<UserDTO> result = new ArrayList<>();

		List<UserEntity> list = ofy().load().type(UserEntity.class).list();
		UserDTO UserDTO = null;
		for (UserEntity e : list) {
			UserDTO = new UserDTO(e);
			result.add(UserDTO);
		}
		return result;
	}

	/**
	 * find the user based on ID
	 * 
	 * @param id
	 * @return
	 */
	public static UserDTO getUserById(long id) {
		if (id <= 0) {
			return null;
		}
		UserEntity user = ofy().load().type(UserEntity.class).id(id).now();
		if (user == null) {
			return null;
		}
		UserDTO dto = new UserDTO(user);
		return dto;
	}

	/**
	 * Change the user using this method
	 * 
	 * @param id
	 */
	public static UserDTO saveUserDetails(UserDTO user) {
		UserEntity u = ofy().load().type(UserEntity.class).id(user.getUserId())
				.now();

		u.setUserId(user.getUserId());
		populateUserDTOToUserEntity(user, u);

		if (user.isOrganization()) {
			OrganizationDTO dto = user.getOrganizationObject();
			OrganizationEntity o = ofy().load().type(OrganizationEntity.class).id(dto.getId()).now();
			o.setId(dto.getId());
			o.setAddress(dto.getAddress());
//			o.setEmail(dto.getEmail());
			o.setOrganizationTitle(dto.getOrganizationTitle());
			o.setPhoneNumber(dto.getPhoneNumber());
			ofy().save().entity(o).now();
			u.setOrganizationId(dto.getId());
		}

		ofy().save().entity(u).now();
		return new UserDTO(u);
	}

	private static void populateUserDTOToUserEntity(UserDTO user, UserEntity u) {
		u.setUserName(user.getUserName());
		u.setMainGoal(user.getMainGoal());
		u.setEmail(user.getEmail());
		u.setPassword(user.getPassword());
		u.setPersonalwebsite(user.getPersonalwebsite());
		u.setAddress(user.getAddress());
		u.setTotalFundedAmount(user.getTotalFundedAmount());
		u.setBirthDate(user.getBirthDate());
		u.setHobbys(user.getHobbys());
		u.setOrganization(user.isOrganization());
		u.setProjectType(user.getProjectType());
		u.setAvailability(user.getAvailability());
	}

	/**
	 * Delete the user using this method
	 * 
	 * @param id
	 */
	public static void deleteUserById(long id) {
		if (id <= 0) {
			return;
		}
		UserEntity user = ofy().load().type(UserEntity.class).id(id).now();
		if (user == null) {
			return;
		}
		ofy().delete().entities(user).now();

	}

	/**
	 * This method is used to create a User
	 * 
	 * @param user
	 * @return
	 */
	public static UserDTO createUser(UserDTO user) {
		UserEntity u = new UserEntity();

		populateUserDTOToUserEntity(user, u);

		if (user.isOrganization()) {
			OrganizationDTO dto = user.getOrganizationObject();
			if (dto != null) {
				OrganizationEntity o = new OrganizationEntity();
				o.setAddress(dto.getAddress());
				// o.setEmail(dto.getEmail());
				o.setOrganizationTitle(dto.getOrganizationTitle());
				o.setPhoneNumber(dto.getPhoneNumber());
				ofy().save().entity(o).now();
				u.setOrganizationId(o.getId());
			}
		}

		ofy().save().entity(u).now();
		return new UserDTO(u);
	}

	/**
	 * This method helps the user to Login into the Application
	 * 
	 * @param email
	 * @param password
	 * @return
	 */
	public static UserDTO login(String email, String password) {

		UserDTO userDTO = null;

		UserEntity userEntity = ofy().load().type(UserEntity.class)
				.filter("email", email).filter("password", password).first()
				.now();

		log.finest("user=" + userEntity);// needs to implement the Logger
											// functionality

		if (userEntity != null) {
			userDTO = new UserDTO(userEntity);
		}
		return userDTO;
	}

	public static UserDTO setUserProjectTypes(long userID, List<String> types) {

		UserEntity user = ofy().load().type(UserEntity.class).id(userID).now();
		user.setProjectType(types);
		ofy().save().entity(user).now();
		UserDTO u = new UserDTO(userID);

		return u;
	}

}
