package sociallinder.dto;

import static com.googlecode.objectify.ObjectifyService.ofy;
import lombok.Data;
import sociallinder.entity.KeyNeedEntity;

/**
 * Created by Timo on 25.10.15.
 */
@Data
public class KeyNeedDTO {

    private Long id;

    private String title;

    private String description;

    private double totalNeeds;

    private double needsMet;

    private String dimension;

   

    public KeyNeedDTO(){}

    public KeyNeedDTO(KeyNeedEntity e){
        id = e.getId();
        title = e.getTitle();
        description = e.getDescription();
        totalNeeds = e.getTotalNeeds();
        needsMet = e.getNeedsMet();
        dimension = e.getDimension();
    }
    
    public KeyNeedDTO(long KeyNeedID){
    	 
    	KeyNeedEntity e = new KeyNeedEntity();
    	e= ofy().load().type(KeyNeedEntity.class).id(KeyNeedID).now();
    	 id = e.getId();
         title = e.getTitle();
         description = e.getDescription();
         totalNeeds = e.getTotalNeeds();
         needsMet = e.getNeedsMet();
         dimension = e.getDimension();
         
    	
    	
    }

    public static KeyNeedDTO createSampleEntity() {
        return new KeyNeedDTO(KeyNeedEntity.createSampleEntity());

    }
}
